<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?=base_url()?>/assets/dashboard/bootstrap/dist/js/tether.min.js"></script>
<script src="<?=base_url()?>/assets/dashboard/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?=base_url()?>/assets/dashboard/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url()?>/assets/dashboard/js/waves.js"></script>
<!-- Flot Charts JavaScript -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/flot/jquery.flot.js"></script>
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<!-- google maps api -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline charts -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART JS -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=base_url()?>/assets/dashboard/js/custom.min.js"></script>
<script src="<?=base_url()?>/assets/dashboard/js/dashboard2.js"></script>
<!--Style Switcher -->
<script src="<?=base_url()?>/assets/dashboard/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
