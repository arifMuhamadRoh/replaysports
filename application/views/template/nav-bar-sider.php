<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
    </span> </div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="#" class="waves-effect"> <span class="hide-menu"> <?=$this->ion_auth->user()->row()->username ?> <span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
            <li class="nav-small-cap m-t-10"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Main Menu</li>

            <?php if ($this->ion_auth->is_admin()): ?>
            <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> Menu Administrator <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=site_url('dashboard/daftarLapangan'); ?>">Daftar Lapangan</a> </li>
                    <li> <a href="<?=site_url('dashboard/daftarPemain'); ?>">Daftar Pemain</a> </li>
                    <li> <a href="<?=site_url('dashboard/daftarWasit'); ?>">Daftar Wasit</a> </li>
                </ul>
            </li>
            <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> Admin Lapangan <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=site_url('dashboard/daftarBooking'); ?>">Daftar Booking</a> </li>
                    <li> <a href="<?=site_url('dashboard/jadwalLapangan'); ?>">Jadwal Lapangan</a> </li>
                    <li> <a href="<?=site_url('dashboard/historyBooking'); ?>">History Booking</a> </li>
                </ul>
            </li>
            <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> User Pemain <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=site_url('dashboard/bookingVenu'); ?>">Booking Venue</a> </li>
                    <li> <a href="<?=site_url('dashboard/bookingMatch'); ?>">Booking Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/searchMatch'); ?>">Search Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/eyesportsLeague'); ?>">Eye Sport League</a> </li>
                    <li> <a href="<?=site_url('dashboard/playasWasit'); ?>">Play As Wasit</a> </li>
                    <li> <a href="<?=site_url('dashboard/historyMatch'); ?>">History Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/rangkingTeam'); ?>">Rangking Team</a> </li>
                </ul>
            </li>
          <?php endif ?>
          <?php if ($this->ion_auth->in_group(array("lapangan"))): ?>
            <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> Admin Lapangan <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=site_url('dashboard/daftarBooking'); ?>">Daftar Booking</a> </li>
                    <li> <a href="<?=site_url('dashboard/jadwalLapangan'); ?>">Jadwal Lapangan</a> </li>
                    <li> <a href="<?=site_url('dashboard/historyBooking'); ?>">History Booking</a> </li>
                </ul>
            </li>
          <?php endif ?>
          <?php if ($this->ion_auth->in_group(array("pemain"))): ?>
            <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> User Pemain <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=site_url('dashboard/bookingVenu'); ?>">Booking Venue</a> </li>
                    <li> <a href="<?=site_url('dashboard/bookingMatch'); ?>">Booking Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/searchMatch'); ?>">Search Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/eyesportsLeague'); ?>">Eye Sport League</a> </li>
                    <li> <a href="<?=site_url('dashboard/playasWasit'); ?>">Play As Wasit</a> </li>
                    <li> <a href="<?=site_url('dashboard/historyMatch'); ?>">History Match</a> </li>
                    <li> <a href="<?=site_url('dashboard/rangkingTeam'); ?>">Rangking Team</a> </li>
                </ul>
            </li>
          <?php endif ?>
            <li><a href="<?=site_url('welcome'); ?>" class="waves-effect"><i class="zmdi zmdi-power zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
        </ul>
    </div>
</div>
