<!DOCTYPE html>
<html lang="en">

<head>
<?php
  $this->load->view('template/style');
?>
</head>

<body>
    <!-- Preloader -->
        <?php
          $this->load->view('template/header');
        ?>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        <!-- Left navbar-header -->
              <?php
                $this->load->view('template/nav-bar-sider');
              ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
              <?php
                $this->load->view('template/page-content');
                echo $content
              ?>
                <!-- /.right-sidebar -->
                <!-- /.container-fluid -->
          <?php
            $this->load->view('template/footer');
          ?>
        <!-- /#wrapper -->
<!-- this is plugins jquery and boostrap -->
<?php
  $this->load->view('template/plugins');
?>
</body>

</html>
