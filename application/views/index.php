<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- website Name -->
    <title>Eye Sports Indonesia</title>
    <link rel="shortcut icon" type="image" href="<?=base_url()?>/assets/welcome/images/logo-1.png"/>

    <!-- Css Files -->
    <link href="<?=base_url()?>/assets/welcome/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/owl.carousel.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/color.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/dl-menu.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/flexslider.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/prettyphoto.css" rel="stylesheet">
    <link href="<?=base_url()?>/assets/welcome/css/responsive.css" rel="stylesheet">


</head>

<body>
    <!--// Main Wrapper \\-->
    <div class="ec-main-wrapper">
        <div class="ec-loading-section">
          <div class="ball-scale-multiple"><div></div><div></div><div></div></div></div>
        <!--// Main Header \\-->
        <header id="ec-header">
            <!--// TopSection \\-->
            <div class="ec-top-strip">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="ec-strip-info">
                                <li>Eye Sport Indonesia</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--// TopSection \\-->
            <!--// Main Header \\-->
            <div class="ec-main-navsection">
                <div class="container">
                    <a href="index.php" class="ec-logo"><img src="assets/welcome/images/logo-1.png" alt=""></a>
                    <div class="ec-right-section">
                        <nav class="ec-navigation">
                            <ul>
                                <li class="active"><a href="index.php">Home</a></li>
                                <li><a href="modal" data-toggle="modal" data-target="#ModalLogin"><i></i> Login & Rigester</a></li>
                            </ul>
                        </nav>

                        <!--// Responsive Menu //-->
                        <div id="as-menu" class="as-menuwrapper">
                            <button class="as-trigger">Open Menu</button>
                            <ul class="as-menu">
                                <li class="active"><a href="index.php">Home</a></li>
                                <li><a href="#">EyeSport League</a></li>
                            </ul>
                        </div>
                        <!--// Responsive Menu //-->
                    </div>
                </div>
            </div>
            <!--// Main Header \\-->
        </header>
        <!--// Main Header \\-->
        <!--// Main Banner \\-->
         <div class="ec-mainbanner">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="assets/welcome/extra-images/banner_1.jpg" alt="">
                        <span class="ec-transparent-color"></span>
                        <div class="ec-caption">
                            <div class="container">
                                <div class="caption-inner-wrap">
                                    <div class="clearfix"></div>
                                    <h1>EYE SPORT</h1>
                                    <p>A Futsal Platform for futsal activists who can help filed booking and match between players & team in online league. unlike most other platform sports that only help field booking.</p>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalLogin">Join Now</button>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <img src="assets/welcome/extra-images/banner_2.jpg" alt="">
                        <span class="ec-transparent-color"></span>
                        <div class="ec-caption">
                            <div class="container">
                                <div class="caption-inner-wrap">
                                    <div class="clearfix"></div>
                                    <h1>EYE SPORT</h1>
                                    <p>A Futsal Platform for futsal activists who can help filed booking and match between players & team in online league. unlike most other platform sports that only help field booking.</p>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalLogin">Join Now</button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

              <!--// Main Banner \\-->
        <!--// Main Content \\-->
        <div class="ec-main-content">
            <!--// Main Section \\-->
            <div class="ec-main-section ec-newsticker-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ec-newsticker">
                                <span class="ec-color"><small></small></span>
                                <ul id="">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--// CopyRight Section \\-->
        <footer id="ec-footer">
            <div class="ec-bottom-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="ec-footer-logo"><img src="assets/welcome/images/footer-logo.png" alt=""></a>
                            <div class="ec-copyright">
                                <p>© copyright 2018 Eye Sport Indonesia</p>
                                 <span>Tanggerang Selatan, Indonesia</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="ec-right-section">
                                <a href="index.php" class="backtop-btn">Back to top <i class="fa fa-caret-up"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--// CopyRight Section \\-->
        </footer>
    </div>

    <!--// Main Wrapper \\-->
    <!-- ModalLogin Box -->
    <div class="modal fade" id="ModalLogin" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="ec-modallogin-form ec-login-popup">
                        <span class="ec-color">Login to Your Account</span>
                        <form method="post" action="<?php echo base_url() ?>">
                            <ul>
                                <li>
                                    <input name="identity" type="text" value="Your Username" onblur="if(this.value == '') { this.value ='Your Username'; }" onfocus="if(this.value =='Your Username') { this.value = ''; }"> </li>
                                <li>
                                    <input name="password" type="password" value="password" onblur="if(this.value == '') { this.value ='password'; }" onfocus="if(this.value =='password') { this.value = ''; }"> </li>
                                <li>
                                    <input type="submit" value="Sign In"> </li>
                            </ul>
                        </form>
                        <p>Not a member yet? <a href="#" style="color:#3498DB;">Signup Now!</a></p>
                    </div>
                    <div class="ec-modallogin-form ec-register-popup">
                        <span class="ec-color">create Your Account today</span>
                        <form>
                            <ul>
                                <li>
                                    <input type="text" value="Your Username" onblur="if(this.value == '') { this.value ='Your Username'; }" onfocus="if(this.value =='Your Username') { this.value = ''; }"> </li>
                                <li>
                                    <input type="text" value="Your E-mail" onblur="if(this.value == '') { this.value ='Your E-mail'; }" onfocus="if(this.value =='Your E-mail') { this.value = ''; }"> </li>
                                <li>
                                    <input type="password" value="password" onblur="if(this.value == '') { this.value ='password'; }" onfocus="if(this.value =='password') { this.value = ''; }"> </li>
                                <li>
                                    <input type="text" value="Confirm Password" onblur="if(this.value == '') { this.value ='Confirm Password'; }" onfocus="if(this.value =='Confirm Password') { this.value = ''; }"> </li>
                                <li>
                                    <input type="submit" value="Create Account"> </li>
                            </ul>
                        </form>
                        <p>Already a member? <a href="#" style="color:#3498DB;">Signin Here!</a></p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- ModalLogin Box -->
    <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?=base_url()?>/assets/welcome/script/jquery.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/modernizr.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/bootstrap.min.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/jquery.dlmenu.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/flexslider-min.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/jquery.prettyphoto.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/waypoints-min.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/owl.carousel.min.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/jquery.countdown.min.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/fitvideo.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/newsticker.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/skills.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/functions.js"></script>
    <script src="<?=base_url()?>/assets/welcome/script/scrolling-nav.js"></script>
</body>

</html>
