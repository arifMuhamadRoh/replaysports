<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//Do your magic here
	}

	public function index()
	{
			$this->login();
	}

	public function login()
	{

			$this->data['title'] = $this->lang->line('login_heading');
			$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
			$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

			if ($this->form_validation->run() === TRUE){
				if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), false)) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                redirect('Welcome', 'refresh');
            }
			} else {
						$data['message'] = $this->session->flashdata('message');
            $data['error']   = $this->session->flashdata('error');
            $this->load->view('index', $data);
			}
	}

	public function logut()
	{
				$this->ion_auth->logout();
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        $data['error']   = $this->session->flashdata('error');
        $data['message'] = $this->session->flashdata('message');
        $this->load->view('index',$data);
	}
}
