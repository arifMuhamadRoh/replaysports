<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index(){
			$d['title'] = "Dashboard";
			$d['sub_title'] = "Home";
				$d['content'] = $this->load->view('content/dash', $d, TRUE);
			$this->load->view('dashboard',$d);
		}

	public function daftarLapangan(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Daftar Lapangan";
				$d['content'] = $this->load->view('content/daftarLapangan', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function daftarPemain(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Daftar Pemain";
				$d['content'] = $this->load->view('content/daftarPemain', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function daftarWasit(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Daftar Wasit";
				$d['content'] = $this->load->view('content/daftarWasit', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function daftarBooking(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Daftar Booking";
				$d['content'] = $this->load->view('content/daftarBooking', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function jadwalLapangan(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Jadwal lapangan";
				$d['content'] = $this->load->view('content/jadwalLapangan', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function historyBooking(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "History Booking";
				$d['content'] = $this->load->view('content/historyBooking', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function bookingVenu(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Booking Venue";
				$d['content'] = $this->load->view('content/bookingVenu', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function bookingMatch(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Booking Match";
				$d['content'] = $this->load->view('content/bookingMatch', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function searchMatch(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Search Match";
				$d['content'] = $this->load->view('content/searchMatch', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function eyesportsLeague(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "EyeSports League";
				$d['content'] = $this->load->view('content/eyesportsLeague', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function playasWasit(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Play As Wasit";
				$d['content'] = $this->load->view('content/playasWasit', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function historyMatch(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "History Match";
				$d['content'] = $this->load->view('content/historyMatch', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

	public function rangkingTeam(){
				$d['title'] = "Dashboard";
				$d['sub_title'] = "Team Rangking";
				$d['content'] = $this->load->view('content/daftarLapangan', $d, TRUE);
				$this->load->view('dashboard',$d);
			}

}
